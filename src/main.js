import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import router from "../router";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";
import VueMaterial from 'vue-material'
import { MdButton, MdContent, MdTabs } from "vue-material/dist/components";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";
Vue.use(VueMaterial)
Vue.use(MdButton);
Vue.use(MdContent);
Vue.use(MdTabs);
Vue.prototype.$axios = axios;
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
