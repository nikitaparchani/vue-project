import Vue from "vue";
import Router from "vue-router";
import skills from "./src/components/skills";
import category from "./src/components/category";
import productDetails from "./src/components/productDetails";

Vue.use(Router);
export default new Router({
  routes: [
    {
      path: "/",
      name: "skills",
      component: skills,
    },
    {
      path: "/categoryList",
      name: "category",
      component: category,
    },
    {
      path: "/productDetails/:id",
      name: "productDetails",
      component: productDetails,
    },
  ],
});
